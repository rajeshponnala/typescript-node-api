 Node Api Server using Typescript
 
 $ For development install packages using:

```bash
   $ npm install
   ```
$ For production install packages using:

```bash
   $ npm install --only=prod
   ```
$ Install type script

```bash
   $ npm install typescript --save-dev
```

$ To install gulp 

```bash
   $ npm install gulp gulp-typescript --save-dev
   ```
   
 $ To install express and middleware

```bash
   $ npm install express body-parser morgan --save
   ```
$ To install a type for a module, prefix its name with @types/
   here installing types for *NODE*,*Express*,*debug*,*Body-Parser*,*morgan*
   ```
    $ npm install @types/node @types/express @types/debug @types/body-parser
    @types/morgan  --save-dev
 ```

$ To install chai, mocha
   ```
    $ npm install mocha chai chai-http --save-dev
    $ npm install @types/mocha @types/chai @types/chai-http --save-dev
 ```
$  install knex globally
   ```
    $ npm install knex -g
 ```
$  to create new migartion (after migration created rename js extension to ts
   and import knex)
   ```
    $ knex migrate:make migration_name
    
   ```
$  to run latest migration
   ```
    $ npm run latest_migration
  
  ```
$  To rollback the last batch of migrations
   ```
    $ npm run rollback
  
  ```
$ To install ts-node (To interpret and transpile our TypeScript in memory as
  the tests are run)
   ```
   $ npm install ts-node --save-dev
 ```
 
 $ To run typescript compiler explicitly 
  ```
   $ node_modules/.bin/tsc
  ```
 
 $ To Build code 
  ```
   $ npm run build
  ```
 
 $ To test code 
  ```
   $ npm test
 ```
 $ To start server 
  ```
   $ npm start
 ```
 
 
 

   
   
   


