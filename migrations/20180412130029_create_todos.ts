import * as knex from 'knex';


exports.up = function(knex: knex, Promise: Promise<any>) {
   return knex.schema.createTable('todos', (table) => {
    table.increments('id').primary();
    table.string('text', 500);
    table.boolean('isCompleted');
    table.boolean('isActive');
    table.timestamps();
   });
};

exports.down = function(knex: knex, Promise: Promise<any>) {
   return knex.schema.dropTable('todos');
};
