import * as knex from 'knex';

exports.up = function(knex: knex, Promise: Promise<any>) {
   return knex.schema.createTable('users', table => {
       table.increments()
       table.string('userName').unique().notNullable()
       table.string('password').notNullable()
       table.boolean('isAdmin').notNullable().defaultTo(false)
       table.timestamps()
   })
};

exports.down = function(knex: knex, Promise: Promise<any>) {
   return knex.schema.dropTable('users')
};
