// Update with your config settings.

const config = {

  development: {
    client: "postgresql",
    connection: {
      database: "Todos",
      password: "Design_20",
      user:     "postgres",
    },
    migrations: {
      directoty: "../dist/migrations",
      tableName: "knex_migrations",
    },
    pool: {
      max: 10,
      min: 2,
    },
  },

  production: {
    client: "postgresql",
    connection: {
      database: "my_db",
      password: "password",
      user: "username",
    },
    migrations: {
      tableName: "knex_migrations",
    },
    pool: {
      max: 10,
      min: 2,
    },
 },

};
export default config;
