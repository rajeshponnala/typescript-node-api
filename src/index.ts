import * as debug from "debug";
import * as http from "http";

import App from "./App";

debug("ts-express:server");

const port = normalizePort(process.env.port || 3000);
App.set("port", port);

const server = http.createServer(App);
server.listen(port);

server.on("error", onError);
server.on("listening", onListening);

function normalizePort(val: number | string): number | string | boolean {
  const portNew: number = typeof val === "string" ? parseInt(val, 0) : val;
  if (isNaN(portNew)) {
    return val;
  } else {
    if (portNew >= 0) {
      return port;
    } else {
      return false;
    }
  }
}

function onError(error: NodeJS.ErrnoException): void {
  if (error.syscall !== "listen") {
    throw error;
  }
  const bind = typeof port === "string" ? "Pipe " + port : "Port " + port;
  switch (error.code) {
    case "EACCES":
      process.exit(1);
      break;
    case "EADDRINUSE":
      process.exit(1);
      break;
    default:
      throw error;
  }
}
function onListening(): void {
  const addr = server.address();
  const bind = typeof addr === "string" ? `pipe ${addr}` : `port ${addr.port}`;
  debug(`Listening on ${bind}`);
}
