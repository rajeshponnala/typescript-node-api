import { sign } from "jsonwebtoken";
import { jwtSettings } from "../authuentication/jwt_config";

export const encode = (user: any, hostInput: string): string => {
    return sign({id: user.id, host: hostInput}, jwtSettings().jwtSecret, {
      expiresIn: "2m",
   });
};
