import * as bodyParser from "body-parser";
import * as express from "express";
import { Router } from "express-serve-static-core";
import * as logger from "morgan";
import * as path from "path";
import AuthRouter from "./routes/AuthRouter";

class App {
  public express: express.Application;

  constructor() {
    this.express = express();
    this.middleware();
    this.routes();
  }

  private middleware(): void {
    this.express.use(logger("dev"));
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
  }

  private routes(): void {
    const router: Router = express.Router();
    router.get("/", (req, res, next) => {
      res.json({
        message: "Hello World",
      });
    });
    this.express.use("/", router);
    this.express.use("/api/v1/Auth", AuthRouter);
  }
}

export default new App().express;
