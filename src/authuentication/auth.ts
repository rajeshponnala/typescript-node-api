import * as knex from "knex";
import * as passport from "passport";
import * as passportJwt from "passport-jwt";
import config from "../../knexfile";
import { jwtSettings } from "./jwt_config";

const jwtsttngs = jwtSettings();

const params = {
  jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: jwtsttngs.jwtSecret,
};

export const auth = async () => {
  const strategy = new passportJwt.Strategy(params, async (payload, done) => {
    const user = await knex(config.development).select(
      "id",
      "userName",
      "isAdmin",
    );
    if (user.length > 0) {
      return done(null, user[0]);
    } else {
        return done(new Error("User Not Found"), null);
    }
  });
  passport.use(strategy);
  return {
    authenticate: () => passport.authenticate("jwt", { session: false }),
    initialize: () => passport.initialize(),
  };
};
