import { hash } from "bcryptjs";
import { NextFunction, Request, Response, Router } from "express";
import * as knex from "knex";
import config from "../../knexfile";
import { asyncHandler } from "../utils/AsyncHandlers";
import { encode } from "../utils/Auth";

export class AuthRouter {
  public router: Router;
  constructor() {
    this.router = Router();
    this.init();
  }
  public async Post(
    req: Readonly<Request>,
    res: Readonly<Response>,
    next: Readonly<NextFunction>,
  ) {
    const passwordHash = await hash(req.body.password, 8);
    const user: any = await knex(config.development)
      .insert({
        created_at: new Date(),
        isAdmin: true,
        password: passwordHash,
        updated_at: new Date(),
        userName: req.body.userName,
      })
      .into("users")
      .returning(["id", "userName", "isAdmin"]);
    res.status(200).json({
      auth: true,
      token: encode(user[0], req.hostname),
    });
  }

  public async Login(
    req: Readonly<Request>,
    res: Readonly<Response>,
    next: Readonly<NextFunction>,
  ) {
    const passwordHash = await hash(req.body.password, 8);
    const user: any = await knex(config.development)
      .select("id")
      .from("users");
    res.status(200).json({
      auth: true,
      token: encode(user[0], req.hostname),
    });
  }

  private init(): void {
    this.router.post("/register", asyncHandler(this.Post));
    this.router.post("/login", asyncHandler(this.Login));
  }
}

export default new AuthRouter().router;
